import Model.Gun;
import com.fasterxml.jackson.core.JsonParseException;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JSONParser {
  private static ObjectMapper objectMapper = new ObjectMapper();

  public static List<Gun> getGuns(File jsonFile)
      throws IOException, JsonParseException, JsonMappingException {
    List<Gun> guns = new ArrayList<>();
    // for Java 1.5+
    Gun response[] = objectMapper.readValue(jsonFile, Gun[].class);
    for (Gun g : response) {
      guns.add(g);
    }
    return guns;
  }
}
