import Comparator.GunComparatorById;
import Model.Gun;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class App {
  public static void main(String[] args) {

    File json = new File("src/main/resources/gun.json");
    File schema = new File("src/main/resources/gunScheme.json");
    try {
      boolean validJson = JSONValidator.validate(json, schema);
      if (validJson) {
        List<Gun> guns = null;
        try {
          guns = JSONParser.getGuns(json);
        } catch (JsonParseException e) {
          e.printStackTrace();
          System.out.println("Json file not parsed!");
        } catch (JsonMappingException e) {
          e.printStackTrace();
          System.out.println("Json file not mapping to Model!");
        } catch (IOException e) {
          e.printStackTrace();
          System.out.println("Json file not found!");
        }
        if (guns != null) {
          Collections.sort(guns, new GunComparatorById());
          System.out.println("List of Gun sorted by iD");
          System.out.println(guns);
        } else {
          System.out.println("No elements in Gun");
        }
      }
    } catch (ProcessingException e) {
      e.printStackTrace();
      System.out.println("Json file not valid!");
    } catch (IOException e) {
      e.printStackTrace();
      System.out.println("File not found!");
    }
  }
}
