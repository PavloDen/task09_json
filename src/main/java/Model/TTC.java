package Model;

public class TTC {
  private RangeParameter rangeParameter;
  private boolean hasMagazine;
  private boolean hasTelescopicSight;

  public TTC() {}

  public TTC(RangeParameter rangeParameter, boolean hasMagazine, boolean hasTelescopicSight) {
    this.rangeParameter = rangeParameter;
    this.hasMagazine = hasMagazine;
    this.hasTelescopicSight = hasTelescopicSight;
  }

  public RangeParameter getRangeParameter() {
    return rangeParameter;
  }

  public void setRangeParameter(RangeParameter rangeParameter) {
    this.rangeParameter = rangeParameter;
  }

  public boolean isHasMagazine() {
    return hasMagazine;
  }

  public void setHasMagazine(boolean hasMagazine) {
    this.hasMagazine = hasMagazine;
  }

  public boolean isHasTelescopicSight() {
    return hasTelescopicSight;
  }

  public void setHasTelescopicSight(boolean hasTelescopicSight) {
    this.hasTelescopicSight = hasTelescopicSight;
  }

  @Override
  public String toString() {
    return "TTC{"
        + "rangeParameter="
        + rangeParameter
        + ", hasMagazine="
        + hasMagazine
        + ", hasTelescopicSight="
        + hasTelescopicSight
        + '}';
  }
}
