package Model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class RangeParameter {
  private int maximumFiringRange;
  private DistanceRange distanceRange;
  private int effectiveFiringRange;

  public RangeParameter() {}

  public RangeParameter(
      int maximumFiringRange, DistanceRange distanceRange, int effectiveFiringRange) {
    this.maximumFiringRange = maximumFiringRange;
    this.distanceRange = distanceRange;
    this.effectiveFiringRange = effectiveFiringRange;
  }

  public int getMaximumFiringRange() {
    return maximumFiringRange;
  }

  public void setMaximumFiringRange(int maximumFiringRange) {
    this.maximumFiringRange = maximumFiringRange;
  }

  public DistanceRange getDistanceRange() {
    return distanceRange;
  }

  public void setDistanceRange(DistanceRange distanceRange) {
    this.distanceRange = distanceRange;
  }

  public int getEffectiveFiringRange() {
    return effectiveFiringRange;
  }

  public void setEffectiveFiringRange(int effectiveFiringRange) {
    this.effectiveFiringRange = effectiveFiringRange;
  }

  @Override
  public String toString() {
    return "RangeParameter{"
        + "maximumFiringRange="
        + maximumFiringRange
        + ", distanceRange="
        + distanceRange
        + ", effectiveFiringRange="
        + effectiveFiringRange
        + '}';
  }
}
