package Model;

public enum DistanceRange {
  LOW,
  MEDIUM,
  HIGH
}
